## v0.11.1 (2022-09-20)

Fifth rolling release.

**Requirements:**
  * [x] OS Linux or Darwin
  * [x] `bash >= 3.0.0`
  * [x] `python >= 3.5.0`

**Bugfix:**
  * [x] Issues #21: Wrong detection of python version 3.10 lower than 3.5

## v0.11.0 (2021-07-27)

Fourth rolling release.

**Requirements:**
  * [x] OS Linux or Darwin
  * [x] `bash >= 3.0.0`
  * [x] `python >= 3.5.0`

**New Features:**
  * [x] template - Initialize project with common required directory and skeleton.
  * [x] export - Archive virtual environment plus project directory.
  * [x] import - Deploy archived virtual environment.
  * [x] edit - Edit config and pre/post of activation/deactivation scripts.
  * [x] support pre/post of activation - Script sourced before/after the current environment is activated.
  * [x] support pre/post of deactivation - Script sourced before/after the current environment is deactivated.

**Enhancement:**
  * [x] remove - Option to remove/delete virtual environment without conformation.

## v0.8.2 (2019-08-27)

Third rolling release.

**Requirements:**
  * [x] OS Linux or Darwin
  * [x] `bash >= 3.0.0`
  * [x] `python >= 3.5.0`

**New Features:**
* [x] Add support for RHEL 8.
* [x] Enhance installer capability:
    * Check for install directory writable.
    * Check for previous installation.
    * If package already download don't redownload.
    * Extract source package directly to install directory.
    * Clean up downloaded source package.

**Fixed:**
* [x] Installer bug fixed:
    * Source package download point to specific tag release.
    * SHA-1 not supported on RHEL8. Moved to SHA-256 which is stronger.
    * Download whole source package tar.gz instead of file by file.

## v0.7.1 (2018-05-07)

Second rolling release.

**Requirements:**
* [x] OS Linux or Darwin
* [x] `bash >= 3.0.0`
* [x] `python >= 3.5.0`

**New Features:**
* [x]  **goto** - Change to predefined directory of active virtual environment.

## v0.6.7 (2018-03-16)

First rolling release.

**Requirements:**
* [x] OS Linux or Darwin
* [x] `bash >= 3.0.0`
* [x] `python >= 3.5.0`

**Features:**
* [x] **create** - Create virtual environment and project directory. Prepare isolated environment via '_venv_' Python module.
* [x]  **remove** - Remove virtual environment and project directory.
* [x]  **list** - List created virtual environment
* [x]  **activate** - Activate virtual environment and automatically change to project directory.
* [x]  **deactivate** - Deactivate current active virtual environment.
