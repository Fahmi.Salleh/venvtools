from setuptools import setup
from os.path import join, dirname

long_description = open(join(dirname(__file__), 'README.md',)).read()
requirements = open(join(dirname(__file__), 'requirements.txt',)).read()

setup(name='webapp',
      version='0.0.1',
      description='The coolest WebApp in the world',
      long_description=readme(),
      classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environmen',
        'Framework :: Bottle',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: MIT License',
        'Operating System :: MacOS :: MacOS X',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
      ],
      keywords='webapp web http www',
      url='http://github.com/storborg/funniest',
      author='Tony Stark',
      author_email='tony.stark@starkindustries.com',
      license='MIT',
      packages=['_projectname'],
      install_requires=requirements.splitlines()
      test_suite='nose.collector',
      tests_require=['nose', 'nose-cover3'],
      entry_points={
          'console_scripts': ['webapp=_projectname._projectname:main'],
      },
      include_package_data=True,
      zip_safe=False)