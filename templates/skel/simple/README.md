# <PROJECT_NAME>

## Introduction

<PROJECT_DESCRIPTION>

## Features

<WHAT_PROJECT_SUPPOSE_TO_DELIVER>

## Prerequisites

<PROJECT_REQUIREMENT>

## Installation

<HOW_TO_QUICKLY_INSTALL>

## Documentation

<LINK_TO_DOCUMENTATION>

## Change Logs

##### v0.0.1 (YYYY-MM-DD) :new: :fire:

- New feature 1.
- Bugfix 1.

See [change log v0.0.1](https://<DOMAIN>/<ACCOUNT>/<PROJECT_NAME>/tags/v0.0.1)

## Download

Latest release: [v0.0.1](https://<DOMAIN>/<ACCOUNT>/<PROJECT_NAME>/-/archive/v0.0.1/<PROJECT_NAME>-v0.0.1.tar.gz)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details

## Authors

* **<AUTHOR_NAME>** (<EMAIL>) - *Initial work* - 

## Acknowledgments

* Script coded due to laziness :sleepy: to type command here and there.