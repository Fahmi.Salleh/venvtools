#!/usr/bin/env python3

import sys
import os
import argparse


__author__ = 'John Doe'
__version__ = '1.0.0'


def greet(args):
    output = '{0}, {1}!'.format(args.greeting, args.name)
    if args.caps:
        output = output.upper()
    print(output)


def main():
    parser = argparse.ArgumentParser(
        prog=os.path.basename(sys.argv[0]),
        description='greeting command'
    )
    parser.add_argument('--version', action='version', version=__version__)
    subparsers = parser.add_subparsers()

    hello_parser = subparsers.add_parser('hello')
    hello_parser.add_argument('name', help='name of the person to greet')
    hello_parser.add_argument('--greeting', default='Hello', help='word to use for the greeting')
    hello_parser.add_argument('--caps', action='store_true', help='uppercase the output')
    hello_parser.set_defaults(func=greet)

    goodbye_parser = subparsers.add_parser('goodbye')
    goodbye_parser.add_argument('name', help='name of the person to greet')
    goodbye_parser.add_argument('--greeting', default='Hello', help='word to use for the greeting')
    goodbye_parser.add_argument('--caps', action='store_true', help='uppercase the output')
    goodbye_parser.set_defaults(func=greet)

    args = parser.parse_args()
    args.func(args)


if __name__ == '__main__':
    main()
