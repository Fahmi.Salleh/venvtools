# Venvtools: Python Virtual Environments Management Made It Easy

## Introduction

Venvtools provide support for Python’s 3.5 official lightweight virtual environments creation '*venv*' module. Venvtools made managing the Python development virtual environment easier with a series of commands.

Script coded with the following values in mind:

*  leveraging on default library/tools
*  less dependency
*  simple to operate

## Features

Currently, venvtools support managing following virtual environment tasks:

* [x]  **create** - Create a virtual environment and a project directory. Prepare an isolated environment via the '*venv*' Python module.
* [x]  **remove** - Remove virtual environment and project directory.
* [x]  **list** - List created virtual environment.
* [x]  **activate** - Activate virtual environment and automatically change to project directory.
* [x]  **deactivate** - Deactivate the current active virtual environment.
* [x]  **goto** - Change to the selected directory of active environment; *environment*, *bin*, *site-packages* and *project*.
* [x]  **template** - Initialize project with common required directory and skeleton. 🆕 🔥
* [x]  **export** - Archive virtual environment plus project directory. 🆕 🔥
* [x]  **import** - Deploy archived virtual environment. 🆕 🔥
* [x]  **support pre/post of activation** - Script sourced before/after the current environment is activated. 🆕 🔥
* [x]  **support pre/post of deactivation** - Script sourced before/after the current environment is deactivated. 🆕 🔥
* [x]  **edit** - Edit pre/post of activation/deactivation scripts. 🆕 🔥

Todo:

* [ ]  **package** - package management intergration. It will differentiate packages installed for development or production. One command to upate all packages.

## Prerequisites

Following prerequisites to run venvtools:

* Bash 3.0 and above.
* Python 3.5 and above.
* Python '*venv*' module.

### Supported Shell

Venvtools is a set of shell functions scripted in Linux shell. It was developed and tested under bash on macOS X High Sierra (10.13).

Why bash? Most of enterprise Linux distributions have bash as their default shell. What about csh, zsh & ksh? No plan to support other shell except zsh, since macOS Big Sur 11.5 move the default shell to zsh.

It may work with other shells, so if you find it does work with other than bash shell please let me know. If you can modify it to work with another shell, without completely rewriting it, let me know I will do my best to include it.

### Supported Python

Why Python 3.5 and above? Venvtool leverage on Python '*venv*' module for the creation of virtual environments. In the Python version, 3.5 module '*venv*' included officially and recommended for creating virtual environments.

What about Python 2.7? The [End Of Life date (EOL, sunset date) for Python 2.7](http://legacy.python.org/dev/peps/pep-0373/) is in 2020. No plan to support Python 2.7 due to EOL just around the corner and limited resources.

## Installation

Following command will run the installation script:

*  Install latest **stable release**:   
   
   `curl -s https://gitlab.com/Fahmi.Salleh/venvtools/raw/v0.11.1/install | bash`
   
*  Install **development release**:   
   
   `curl -s https://gitlab.com/Fahmi.Salleh/venvtools/raw/master/install | bash -s dev`

For [manual installation](https://gitlab.com/Fahmi.Salleh/venvtools/-/wikis/4.-Installation#manual-installation) please refer to the documentation.

## Documentation

See the [wiki](https://gitlab.com/Fahmi.Salleh/venvtools/-/wikis/1.-Home) for more information.

## Change Logs

Latest release:


##### v0.11.1 (2022-09-20) :new: :fire:

- Fifth rolling release.
- Bugfix Issues #21: Wrong detection of python version 3.10 lower than 3.5

See [changelog v0.11.1](https://gitlab.com/Fahmi.Salleh/venvtools/tags/v0.11.1)


##### v0.11.0 (2021-07-27) :new: :fire:

- Fourth rolling release.
- Support for action *template*, *edit*, *export*, and *import*.
- Support for pre/post of activation/deactivation scripts.
- Option to *remove* virtual environment without conformation.

See [changelog v0.11.0](https://gitlab.com/Fahmi.Salleh/venvtools/tags/v0.11.0)


##### v0.8.2 (2019-08-27)

- Third rolling release.
- Support RHEL 8.
- Installer enhancement and bug fix.

See [changelog v0.8.2](https://gitlab.com/Fahmi.Salleh/venvtools/tags/v0.8.2)


##### v0.7.1 (2018-05-07)

- Second rolling release.
- Support action *goto* - Change to predefined directory of active virtual environment.

See [changelog v0.7.1](https://gitlab.com/Fahmi.Salleh/venvtools/tags/v0.7.1)


##### v0.6.7 (2018-03-16)

- First rolling release.
- Support actions *create*, *list*, *remove*, *activate*, *deactivate* virtual environment.

See [change log v0.6.7](https://gitlab.com/Fahmi.Salleh/venvtools/tags/v0.6.7)

See [full changelog](CHANGELOG.md).

## Download
Latest release: [v0.11.0](https://gitlab.com/Fahmi.Salleh/venvtools/-/archive/v0.11.0/venvtools-v0.11.0.tar.gz)

Previous release: [v0.8.2](https://gitlab.com/Fahmi.Salleh/venvtools/-/archive/v0.8.2/venvtools-v0.8.2.tar.gz)

All release: [all](https://gitlab.com/Fahmi.Salleh/venvtools/tags)

## License

This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details.

## Authors

* **Fahmi Salleh** (fahmie[at]gmail.com)

## Acknowledgments

* Script coded due to laziness :sleepy: to type command here and there.
