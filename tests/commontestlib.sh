#!/usr/bin/env bash
# -*- mode: bash-script -*-

TEST_TOTAL=0
TEST_PASSED=0
TEST_FAILED=0

[[ ${DEBUG} == "yes" ]] && OUTPUT="${PWD}/DEBUG.log" || OUTPUT=/dev/null

function is_file {
    local result
    [[ -f "${1}" ]] && result=0 || result=1
    echo "${result}"
}

function is_dir {
    local result
    [[ -d "${1}" ]] && result=0 || result=1
    echo "${result}"
}

function is_variable_set {
    local result
    [[ -n "${1}" ]] && result=0 || result=1
    echo "${result}"
}

function is_variable_unset {
    local result
    [[ -z "${1}" ]] && result=0 || result=1
    echo "${result}"
}

function is_same {

    [[ "${1}" == "${2}" ]] && result=0 || result=1
    echo "${result}"
}

function is_register {
    echo "$(grep -q "^${1}" "${VENVTOOLS_ENV}"; echo "${?}")"
}

#=== FUNCTION =================================================================
# name  : dir_cleanup
# desc  : Delete all files and directory
# param1: {1} directory
# return: null
#==============================================================================
function dir_cleanup {
    [[ $(ls -A "${1}") ]] && rm -r "${1}"/{*,.[!.]*}
}

#=== FUNCTION =================================================================
# name  : case_title
# desc  : Print test case title
# param1: {1} test case title
# return: null
#==============================================================================
function case_title {
    if [[ $(is_variable_unset "${TEST_START}") -eq 0 ]] ; then
        TEST_START=$(date +%s)
        CASE_TITLE=$(echo "${1}" | tr '[:lower:]' '[:upper:]')
    fi

    printf "%s\n" " "
    printf "%s\n" "=============================================================================="
    printf "%-55s %s\n" "${CASE_TITLE}" "$(date '+%F %r')"
    printf "%s\n" "=============================================================================="
   # printf "%s\n" "$(date '+%F %r')"
    #printf "%s\n" "------------------------------------------------------------------------------"
}

#=== FUNCTION =================================================================
# name  : case_title
# desc  : Print test case title
# param1: {1} test case title
# return: null
#==============================================================================
function case_subtitle {
    printf "%s\n" " "
    #printf "%s\n" "=============================================================================="
    printf "%s\n" "$(echo ${1} | tr '[:lower:]' '[:upper:]')"
    printf "%s\n" "------------------------------------------------------------------------------"
}

#=== FUNCTION =================================================================
# name  : case_result
# desc  : Print test case result
# param1: {1} test case description
# param2: {2} test case result
# return: null
#==============================================================================
function case_result {
    local tag
    TEST_TOTAL=$((TEST_TOTAL + 1))

    [[ ${2} == "0" ]] && { tag="PASS"; TEST_PASSED=$((TEST_PASSED + 1)); } || { tag="FAIL"; TEST_FAILED=$((TEST_FAILED + 1)); }
    printf "%-70s| %s |\n" "${1}" "${tag}"
    [[ ${2} == "0" ]] || printf "[error]: %s \n" "${2}"
    printf "%s\n" "------------------------------------------------------------------------------"
}

#=== FUNCTION =================================================================
# name  : case_result
# desc  : Print test case result
# param1: {1} test case description
# param2: {2} test case result
# return: null
#==============================================================================
function case_summary {
    TEST_END=$(date +%s)
    TIME_TAKEN=$((TEST_END - TEST_START))
    [[ ${TEST_FAILED} -eq 0 ]] && tag="PASS" || tag="FAIL"
    printf "%s\n" ""
    printf "%s\n" "=============================================================================="
    printf "%-70s| %s |\n" "${CASE_TITLE}" "${tag}"
    printf "%s\n" "------------------------------------------------------------------------------"
    printf "TOTAL: %-10s PASSED: %-10s FAILED: %-10s ELAPSED: %s seconds\n" "${TEST_TOTAL}" "${TEST_PASSED}" "${TEST_FAILED}" "${TIME_TAKEN}"
    printf "%s\n" "=============================================================================="
}