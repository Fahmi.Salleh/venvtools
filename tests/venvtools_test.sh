#!/usr/bin/env bash
# -*- mode: bash-script -*-

DEBUG="no"

### SETTINGS ###

# shellcheck source=commontestlib.sh
source "commontestlib.sh"

# shellcheck disable=SC1091
source "${HOME}/Workspaces/Projects/venvtools/venvtools"


### VARIABLES ###

TEST_VIRTUALENV="testenv-${RANDOM}"


### KEYWORDS ###

#=== FUNCTION =================================================================
# name  : verify_template
# desc  : Check required files nad directory for each template define.
# param1: {1} template
# param1: {2} project directory
# return: PASS=0 FAIL=Error Message
#==============================================================================
function verify_template {
    local result errmsg=""

    # All template common files
    [[ "$(is_file "${2}/LICENSE")" -eq 0 ]] || errmsg=" LICENSE"
    [[ "$(is_file "${2}/README.md")" -eq 0 ]] || errmsg="${errmsg} README.md"
    [[ "$(is_file "${2}/requirements.txt")" -eq 0 ]] || errmsg="${errmsg} requirements.txt"
    [[ "$(is_file "${2}/setup.py")" -eq 0 ]] || errmsg="${errmsg} setup.py"
    [[ "$(is_file "${2}/.gitignore")" -eq 0 ]] || errmsg="${errmsg} .gitignore"

    # CLI template common files
    if [[ "${1}" == "simple" ]]; then
            [[ "$(is_file "${2}/${TEST_VIRTUALENV}.py")" -eq 0 ]] || errmsg="${errmsg} ${TEST_VIRTUALENV}.py"
            [[ "$(is_file "${2}/${TEST_VIRTUALENV}_test.py")" -eq 0 ]] || errmsg="${errmsg} ${TEST_VIRTUALENV}_test.py"
    fi

    # CLI, APP and WEB template common files
    if [[ "${1}" == "cli" ]] || [[ "${1}" == "app" ]] || [[ "${1}" == "web" ]]; then
            [[ "$(is_dir "${2}/${TEST_VIRTUALENV}")" -eq 0 ]] || errmsg="${errmsg} ${TEST_VIRTUALENV}"
            [[ "$(is_file "${2}/${TEST_VIRTUALENV}/__init__.py")" -eq 0 ]] || errmsg="${errmsg} ${TEST_VIRTUALENV}/__init__.py"
            [[ "$(is_file "${2}/${TEST_VIRTUALENV}/${TEST_VIRTUALENV}.py")" -eq 0 ]] || errmsg="${errmsg} ${TEST_VIRTUALENV}/${TEST_VIRTUALENV}.py"
            [[ "$(is_dir "${2}/tests")" -eq 0 ]] || errmsg="${errmsg} tests"
            [[ "$(is_file "${2}/tests/__init__.py")" -eq 0 ]] || errmsg="${errmsg} test/__init__.py"
            [[ "$(is_file "${2}/tests/${TEST_VIRTUALENV}_test.py")" -eq 0 ]] || errmsg="${errmsg} test/${TEST_VIRTUALENV}_test.py"
    fi

    # WEB template common files
    if [[ "${1}" == "web" ]]; then
            [[ "$(is_dir "${2}/docs")" -eq 0 ]] || errmsg="${errmsg} docs"
    fi

    [[ -z $errmsg ]] && result="0" || result="Missing template files or directories! [${errmsg} ]"

    echo "${result}"
}

#=== FUNCTION =================================================================
# name  : verify_license
# desc  : Ensure correct license for related file.
# param1: {1} license
# param1: {2} project directory
# return: PASS=0 FAIL=Error Message
#==============================================================================
function verify_license {
    local license
    case "${1}" in
        "agpl") license="GNU AFFERO GENERAL PUBLIC LICENSE" ;;
        "apache") license="Apache License" ;;
        "gpl") license="GNU GENERAL PUBLIC LICENSE" ;;
        "lgpl") license="GNU LESSER GENERAL PUBLIC LICENSE" ;;
        "mit") license="MIT License" ;;
        "mozilla") license="Mozilla Public License" ;;
        "unlicense") license="free and unencumbered" ;;
    esac
    #cat "${2}/LICENSE"
    if grep -q "${license}" "${2}/LICENSE"; then
        result=0
    else
        result="Missing license in ${2}/LICENSE file: ${license}"
    fi

    echo "${result}"
}


#=== FUNCTION =================================================================
# name  : verify_export
# desc  : Check required and common file collected when export.
# param1: {1} path to extracted export directory
# param1: {2} test virtual environment
# return: PASS=0 FAIL=Error Message
#==============================================================================
function verify_export {
    local result errmsg

    # All export common files
    [[ "$(is_file "${1}/venvtools.info")" -eq 0 ]] || errmsg=" venvtools.info"
    [[ "$(is_dir "${1}/${2}")" -eq 0 ]] || errmsg="${errmsg} ${2}"
    [[ "$(is_file "${1}/${2}/.gitignore")" -eq 0 ]] || errmsg="${errmsg} .gitignore"
    [[ "$(is_file "${1}/${2}/LICENSE")" -eq 0 ]] || errmsg="${errmsg} LICENSE"
    [[ "$(is_file "${1}/${2}/README.md")" -eq 0 ]] || errmsg="${errmsg} README.md"
    [[ "$(is_file "${1}/${2}/requirements.txt")" -eq 0 ]] || errmsg="${errmsg} requirements.txt"
    [[ "$(is_file "${1}/${2}/setup.py")" -eq 0 ]] || errmsg="${errmsg} setup.py"

    [[ -z $errmsg ]] && result="0" || result="Missing export files or directories! [${errmsg} ]"

    echo "${result}"
}

#=== FUNCTION =================================================================
# name  : verify_venvinfo
# desc  : Check required information collected for export environment.
# param1: {1} path to extracted export directory
# param1: {2} test virtual environment
# return: PASS=0 FAIL=Error Message
#==============================================================================
function verify_venvinfo {
    local result errmsg

    # All export required parameters
    #[[ "$(is_file "${1}/venvtools.info")" -eq 0 ]] || errmsg=" VENV_NAME"
    if ! grep -q "VENV_NAME=${2}" "${1}/venvtools.info"
    then
        errmsg=" VENV_NAME"
    fi

    if ! grep -q "VENV_PROJECT=${2}" "${1}/venvtools.info"
    then
        errmsg=" VENV_PROJECT"
    fi

    if ! grep -q "VENV_PACKAGES=" "${1}/venvtools.info"
    then
        errmsg=" VENV_PACKAGES"
    fi

    [[ -z $errmsg ]] && result="0" || result="Missing virtual environment required info! [${errmsg} ]"

    echo "${result}"
}


### TEST CASES ###

case_title "Venvtools CLI Functional Test"

# ====================================
# CASE 01: Init: Check confi, env file
# ====================================
case_subtitle "INIT FUNCTION"
# Check config, env file
case_result "Check venvtools config file is exists." "$(is_file "${VENVTOOLS_CFG}")"
case_result "Check venvtools environment list file is exists." "$(is_file "${VENVTOOLS_ENV}")"

# ==============================
# CASE 02: Create: Check envlist
# ==============================
case_subtitle "CREATE FUNCTION"
# Check
venvtools create "${TEST_VIRTUALENV}" > "${OUTPUT}" 2>&1
case_result "Check virtual environment is register." "$(is_register "${TEST_VIRTUALENV}")"

venvdir=$(awk -F':' "/^${TEST_VIRTUALENV}:/ { print \$2 }" "${VENVTOOLS_ENV}")
case_result "Check virtual environment directory creation." "$(is_dir "${venvdir}")"

projectdir=$(awk -F':' "/^${TEST_VIRTUALENV}:/ { print \$3 }" "${VENVTOOLS_ENV}")
case_result "Check project directory creation." "$(is_dir "${projectdir}")"

# ==============================
# CASE 02: List: Check envlist
# ==============================
case_subtitle "LIST FUNCTION"

if venvtools list | grep -q "${TEST_VIRTUALENV}"; then
    inlist=0
else
    inlist="${TEST_VIRTUALENV} not in the environment list."
fi

case_result "Check virtual environment is listed." "${inlist}"

# ====================================================
# CASE 03: Activate: Check Env Dir, Check Project Dir
# ====================================================
case_subtitle "ACTIVATE FUNCTION"

venvtools activate ${TEST_VIRTUALENV}

case_result "Check active virtual environment variable set." "$(is_variable_set "${VIRTUAL_ENV}")"
# Check directory
case_result "Check active virtual environment directory." "$(is_dir "${VIRTUAL_ENV}")"
case_result "Check active project directory." "$(is_dir "${PWD}")"

ls -l "${projectdir}"

# ===================================================
# CASE 04: Template: Check template and license copy
# ===================================================
case_subtitle "TEMPLATE FUNCTION"

# check template simple
venvtools template simple >> "${OUTPUT}" 2>&1
case_result "Check simple template creation." "$(verify_template "simple" "${projectdir}")"
dir_cleanup "${projectdir}"

# check template app
venvtools template app >> "${OUTPUT}" 2>&1
case_result "Check app template creation." "$(verify_template "app" "${projectdir}")"
dir_cleanup "${projectdir}"

# check template cli
venvtools template cli >> "${OUTPUT}" 2>&1
case_result "Check cli template creation." "$(verify_template "cli" "${projectdir}")"
dir_cleanup "${projectdir}"

# check template web
venvtools template web >> "${OUTPUT}" 2>&1
case_result "Check web template creation." "$(verify_template "web" "${projectdir}")"
dir_cleanup "${projectdir}"

# check license agpl
venvtools template simple agpl >> "${OUTPUT}" 2>&1
case_result "Check agpl license creation." "$(verify_license "agpl" "${projectdir}")"
dir_cleanup "${projectdir}"

# check license apache
venvtools template simple apache >> "${OUTPUT}" 2>&1
case_result "Check apache license creation." "$(verify_license "apache" "${projectdir}")"
dir_cleanup "${projectdir}"

# check license gpl
venvtools template simple gpl >> "${OUTPUT}" 2>&1
case_result "Check gpl license creation." "$(verify_license "gpl" "${projectdir}")"
dir_cleanup "${projectdir}"

# check license lgpl
venvtools template simple lgpl >> "${OUTPUT}" 2>&1
case_result "Check lgpl license" "$(verify_license "lgpl" "${projectdir}")"
dir_cleanup "${projectdir}"

# check license mit
venvtools template simple mit >> "${OUTPUT}" 2>&1
case_result "Check mit license" "$(verify_license "mit" "${projectdir}")"
dir_cleanup "${projectdir}"

# check license mozilla
venvtools template simple mozilla >> "${OUTPUT}" 2>&1
case_result "Check mozilla license" "$(verify_license "mozilla" "${projectdir}")"
dir_cleanup "${projectdir}"

# check license unlicense
venvtools template simple unlicense >> "${OUTPUT}" 2>&1
case_result "Check unlicense license" "$(verify_license "unlicense" "${projectdir}")"

# ====================================================
# CASE 05: goto: Check
# ====================================================
case_subtitle "GOTO FUNCTION"

venvtools goto environment >> "${OUTPUT}" 2>&1
case_result "Check change directory to the environment." "$(is_same "$(pwd)" "${VIRTUAL_ENV}")"

venvtools goto bin >> "${OUTPUT}" 2>&1
case_result "Check change directory to environment's bin." "$(is_same "$(pwd)" "${VIRTUAL_ENV}/bin")"

venvtools goto site-packages >> "${OUTPUT}" 2>&1
pyver=$(python -c 'import sys; print(".".join(map(str, sys.version_info[:2])))')
case_result "Check change directory to environment's site-packages." "$(is_same "$(pwd)" "${VIRTUAL_ENV}/lib/python${pyver}/site-packages")"

venvtools goto project >> "${OUTPUT}" 2>&1
case_result "Check change directory to the project." "$(is_same "$(pwd)" "${projectdir}")"

# ====================================================
# CASE 06: edit: Check
# ====================================================
# TODO: IN FUTURE

# ====================================================
# CASE 07: export: Check
# ====================================================
case_subtitle "EXPORT FUNCTION"

expname=$(venvtools export  | grep exported | sed 's/.*\[//; s/\].*//')
expfile="${projectdir}/${expname}"

case_result "Check exported compressed files exist." "$(is_file "${expfile}")"

workingdir="/tmp/venvtest"
archievedir="${expname%.*}"
mkdir -p "${workingdir}"
tar xfz "${expfile}" -C "${workingdir}"

# Move for import test case
mv "${expfile}" "${workingdir}"

case_result "Check exported virtual environment info." "$(verify_venvinfo "${workingdir}/${archievedir}" "${TEST_VIRTUALENV}")"

case_result "Check exported compressed file content." "$(verify_export "${workingdir}/${archievedir}" "${TEST_VIRTUALENV}")"

# ====================================================
# CASE 09: Dectivate: Check
# ====================================================
case_subtitle "DEACTIVATE FUNCTION"

venvtools deactivate >> "${OUTPUT}" 2>&1

# Check directory
case_result "Check active virtual environment variable unset." "$(is_variable_unset "${VIRTUAL_ENV}")"
#case_result "Check active project directory" "$(is_dir "${PWD}")"

# ====================================================
# CASE 10: remove: Check
# ====================================================
case_subtitle "REMOVE FUNCTION"

venvtools remove "${TEST_VIRTUALENV}" -y >> "${OUTPUT}" 2>&1

[[ $(is_register "${TEST_VIRTUALENV}") == 0 ]] && is_remove="Virtual environment remove failed!" || is_remove=0
case_result "Check virtual environment deleted from environment list." "${is_remove}"

[[ $(is_dir "${venvdir}") -eq 0 ]] && venvdir_rm="Project directory remove failed!" || venvdir_rm=0
case_result "Check directory environment deleted." "${venvdir_rm}"

[[ $(is_dir "${projectdir}") -eq 0 ]] && projectdir_rm="Project directory remove failed!" || projectdir_rm=0
case_result "Check directory project deleted." "${projectdir_rm}"

# ====================================================
# CASE 08: import: Check
# ====================================================
case_subtitle "IMPORT FUNCTION"

venvtools import "${workingdir}/${expname}" >> "${OUTPUT}" 2>&1

case_result "Check virtual environment is register." "$(is_register "${TEST_VIRTUALENV}")"

venvdir=$(awk -F':' "/^${TEST_VIRTUALENV}:/ { print \$2 }" "${VENVTOOLS_ENV}")
case_result "Check virtual environment directory creation." "$(is_dir "${venvdir}")"

projectdir=$(awk -F':' "/^${TEST_VIRTUALENV}:/ { print \$3 }" "${VENVTOOLS_ENV}")
case_result "Check project directory creation." "$(is_dir "${projectdir}")"


# ====================================================
# CASE 08: help: Check
# ====================================================
case_subtitle "HELP FUNCTION"

if venvtools help | grep -q "Usage"
then
    helpresult=0
else
    helpresult="Inccorect help displayed!"
fi

case_result "Check the help message." "${helpresult}"

# ====================================================
# CASE 08: version: Check
# ====================================================
case_subtitle "VERSION FUNCTION"

if venvtools help | grep -q -E '(PROGRAM|VERSION|venvtools)'
then
    verresult=0
else
    verresult="Inccorect version displayed!"
fi

case_result "Check the version message." "${verresult}"

# ====================================================
# Summary
# ====================================================
case_summary

# ====================================================
# CLEAN UP
# ====================================================
venvtools remove "${TEST_VIRTUALENV}" -y >> "${OUTPUT}" 2>&1